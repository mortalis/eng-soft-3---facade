# Funcionamento

- <strong>O Padrão de Projeto Facade</strong> oculta toda a complexidade de uma ou mais classes através de uma Facade (Fachada). A intenção desse Padrão de Projeto é  simplificar uma interface. Existem outros dois Padrões de Projetos (<a href="http://www.devmedia.com.br/padrao-de-projeto-decorator-em-java/26238" target="_blank" title="Padrão de Projeto Decorator em Java">Decorator</a> e <a href="http://www.devmedia.com.br/padrao-de-projeto-adapter-em-java/26467" target="_blank" title="Padrão de Projeto Adapter em Java">Adapter</a>), que possuem similaridades com o Padrão Facade, porem existem diferenças em relação a este padrão.

- A definição oficial do padrão Facade é: “O Padrão Facade fornece uma interface unificada para um conjunto de interfaces em um subsistema. O Facade define uma interface de nível mais alto que facilita a utilização do subsistema”.


- <p>O Diagrama de classe abaixo mostra mais detalhes sobre o funcionamento do padr&atilde;o Facade.</p>

<a><img src="http://videos.web-03.net/artigos/Higor_Medeiros/PadraoFacade_Java/PadraoFacade_Java1.jpg" alt="Gitter"></a>

- <p><strong>Figura 1:</strong>&nbsp;Diagrama de classe do Padr&atilde;o Facade</p>
- <p>No diagrama de classe acima tem-se o Client que &eacute; quem acessa a Facade que, por sua vez, &eacute; uma interface simplificada do subsistema, sendo esta unificada e f&aacute;cil de ser utilizada pelo cliente. Abaixo da Facade tem-se as classes do sistema que podem ser chamados diretamente, mas est&atilde;o sendo agrupados na Facade.</p>

***__Exemplo de Implementação__***

<p>Segue abaixo um exemplo de implementa&ccedil;&atilde;o em Java utilizando o Padr&atilde;o Facade.</p>

<p><strong>Listagem 1:</strong>&nbsp;Exemplo de implementa&ccedil;&atilde;o do Padr&atilde;o Facade</p>

```java
public class Cpu {
 
    public void start() {
        System.out.println("inicialização inicial");
    }
    public void execute() {
        System.out.println("executa algo no processador");
    }
    public void load() {
        System.out.println("carrega registrador");
    }
    public void free() {
        System.out.println("libera registradores");
    }
}
 
public class Memoria {
    public void load(int position, String info) {
        System.out.println("carrega dados na memória");
    }
    public void free(int position, String info) {
        System.out.println("libera dados da memória");
    }
}
 
public class HardDrive {
    public void read(int startPosition, int size) {
        System.out.println("lê dados do HD");
    }
    public void write(int position, String info) {
        System.out.println("escreve dados no HD");
    }
}
 
public class ComputadorFacade {
    private Cpu cpu = null;
    private Memoria memoria = null;
    private HardDrive hardDrive = null;
 
    public ComputadorFacade(Cpu cpu,
                    Memoria memoria,
                    HardDrive hardDrive) {
        this.cpu = cpu;
        this.memoria = memoria;
        this.hardDrive = hardDrive;
    }
 
    public void ligarComputador() {
        cpu.start();
        String hdBootInfo = hardDrive.read(BOOT_SECTOR, SECTOR_SIZE);
        memoria.load(BOOT_ADDRESS, hdBootInfo);
        cpu.execute();
        memoria.free(BOOT_ADDRESS, hdBootInfo);
    }
}
```

<p>No exemplo acima podemos notar a quantidade de classes e m&eacute;todos envolvidos quando precisamos inicializar o computador. Toda essa complexidade &eacute; exposta ao cliente que poderia chamar todas essas classes e cada um dos m&eacute;todos das classes para realizar a tarefa de inicializar o computador. No entanto, ao usar uma Facade encapsulamos essa complexidade oferecendo uma interface simples e unificada ao cliente evitando acoplamento e complexidade. Apenas chamando o m&eacute;todo ligarComputador() da classe ComputadorFacade tem-se uma interface simples que diz o que ela faz exatamente, sem expor a complexidade envolvida na opera&ccedil;&atilde;o.</p>

<p>Nota-se que todas as chamadas que est&atilde;o no Facade poderiam ser feitas uma a uma no cliente, por&eacute;m isso gera muito acoplamento e complexidade para o cliente, por isso a Facade simplifica e unifica esse conjunto de classes que gera muita complexidade.</p>

## Vantagens de Usar o Padr&atilde;o Facade

<p>O Padr&atilde;o Facade nos permite desconectar a implementa&ccedil;&atilde;o do cliente de qualquer subsistema. Assim, se quis&eacute;ssemos acrescentar novas funcionalidades no subsistema seria necess&aacute;rio apenas alterar a Facade ao inv&eacute;s de alterar diversos pontos do sistema. Al&eacute;m disso, o padr&atilde;o Facade simplifica uma interface tornando-a muito mais simples e unifica um conjunto de classes mais complexas que pertencem a um subsistema.</p>

# Desvantagens

- a classe fachada pode crescer descontroladamente
- promovem um acoplamento fraco entre o subsistema e seus clientes.

## Conclus&atilde;o

<p>O Padr&atilde;o Facade &eacute; utilizado quando precisamos simplificar e unificar uma interface grande ou um conjunto complexo de interfaces. Uma das vantagens do padr&atilde;o Facade &eacute; desconectar o cliente de um subsistema complexo, conforme pode ser visto no diagrama de classes. Um sistema pode ter diversos Facades simplificando diversos pontos do programa.</p>

<p>&nbsp;</p>

## Fonte
<p>https://www.devmedia.com.br/padrao-de-projeto-facade-em-java/26476</p>


